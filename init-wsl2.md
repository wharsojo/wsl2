* https://scottspence.com/posts/use-chrome-in-ubuntu-wsl
* https://askubuntu.com/questions/1385703/launch-xfce4-or-other-desktop-in-windows-11-wslg-ubuntu-distro
* https://www.digitalocean.com/community/tutorials/how-to-set-up-a-remote-desktop-with-x2go-on-ubuntu-20-04

Install Codium
```bash
wget -qO - https://gitlab.com/paulcarroty/vscodium-deb-rpm-repo/raw/master/pub.gpg \
    | gpg --dearmor \
    | sudo dd of=/usr/share/keyrings/vscodium-archive-keyring.gpg
echo 'deb [ signed-by=/usr/share/keyrings/vscodium-archive-keyring.gpg ] https://download.vscodium.com/debs vscodium main' \
    | sudo tee /etc/apt/sources.list.d/vscodium.list
sudo apt update && sudo apt -y upgrade && sudo apt -y autoremove
sudo apt install codium
```
Install google-chrome
```bash
wget https://dl.google.com/linux/direct/google-chrome-stable_current_amd64.deb
sudo apt -y install ./google-chrome-stable_current_amd64.deb
```
Run XFCE4
```bash
Xwayland :1 &
WAYLAND_DISPLAY= DISPLAY=:1 xfce4-session
kill $(ps aux | grep 'Xwayland' | awk '{print $2}')
# delete XServer
# ps -x | grep Xwayland
```
Common installation
```bash
sudo apt install git zsh build-essential libssl-dev \
    mesa-common-dev libx11-dev libxi-dev libxcursor-dev \
    python-is-python3 python3-pip python3-venv docker.io \
    xwayland xfce4 remmina -y

echo "PATH=\$PATH:~/.local/bin" >> ~/.zshrc
chsh -s $(which zsh)
curl --proto '=https' -fLsS https://rossmacarthur.github.io/install/crate.sh \
    | bash -s -- --repo rossmacarthur/sheldon --to ~/.local/bin

# configure windows-terminal to run Ubuntu & restart terminal
sheldon init --shell zsh
# Initialized ~/.config/sheldon/plugins.toml

# plugins
[plugins.base16]
github = "chriskempson/base16-shell"

[plugins.oh-my-zsh]
github = "ohmyzsh/ohmyzsh"

[plugins.powerlevel10k]
github = "romkatv/powerlevel10k"

[plugins.zsh-autosuggestions]
github = "zsh-users/zsh-autosuggestions"
use = ["{{ name }}.zsh"]

[plugins.autojump]
github = "wting/autojump"
dir = "bin"
apply = ["PATH", "source"]
```

